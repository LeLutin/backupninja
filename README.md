# backupninja

#### Table of Contents

1. [Description](#description)
2. [Setup](#setup)
    * [Setup requirements](#setup-requirements)
    * [Getting started](#getting-started)
3. [Usage](#usage)
    * [Configure backupninja](#configure-backupninja)
    * [Scheduling backups](#scheduling-backups)
    * [Backup accounts](#backup-accounts)
4. [Reference](#reference)
5. [Limitations](#limitations)
6. [Development](#development)

## Description

This module manages your backups using the backupninja program, with the option of creating remote accounts on a designated server for storing backup data.

It installs and configures backupninja, including its cron job. It can be used to deploy configurations for the various backup handlers it supports, such as MySQL, PostgreSQL, duplicity and rsync along with the package dependencies, if any, of these backup tools. It can also export user, ssh key and file resources to a backup server, creating a remotely accessible storage space allowing clients to upload their backups on a remote server.

## Setup

### Setup Requirements

This module depends on the [puppetlabs-stdlib](https://forge.puppet.com/puppetlabs/stdlib) module.

Users upgrading from the pre-4.0.0 version of this module should refer to the UPGRADING file which describes the changes introduced in this new release.

### Getting started

`class { 'backupninja': }` will install backupninja with a default configuration, along with an hourly cron job. It won't actually do any backups until a backup action is configured. This is accomplished declaring one or more of the backupninja::action defined types.

For example, this will install a backup action in `/etc/backup.d` which will create compressed SQL dumps of all MySQL/MariaDB databases present on this node:

``` puppet
::backupninja::action::mysql { 'mysql':
  sqldump => true
}
```

By default, backup actions are run every day at 1 AM.

## Usage

### Configure backupninja

In the example below, backupninja will be configured to run every night at 4 AM, sending an email report to 'admin@example.net'.

``` puppet
class { 'backupninja':
  when          => 'everyday at 04',
  reportemail   => 'admin@example.net',
  reportsuccess => true,
}
```

Refer to the backupninja manpage for details regarding how to correctly format values for the `when` parameter.

### Scheduling backups

By default, backupninja checks whether to run jobs once every hour, on the hour, via its cron job. Therefore, if you specify minutes in the `when` parameter value (eg. 'everyday at 04:30'), it will be considered valid but the minutes compoment will be ignored and backupninja will run at 4 AM.

The examples below illustrate alternative ways to organize backup scheduling using backupninja.

#### Fixed time

This will run backupninja every day at 3:45 AM.

``` puppet
class { 'backupninja':
  when         => 'manual',
  cron_minute  => '45',
  cron_hour    => '3',
  cron_command => "${backupninja::executable} --now"
}
```

To have different fixed times per node, you may, for example, derive the minute component from the `fqdn_rand(59)` function.


#### Random delay

This action will add a random delay of up to one hour at the beginning of the backup run. Useful if you want to avoid clients hitting the backup server all at once.

``` puppet
class { 'backupninja':
  when => 'everyday at 04',
}
backupninja::action::sh { 'delay':
  priority => 0,
  content => 'perl -e "sleep int(rand(3600))"',
}
```

### Backup accounts

This module allows you to provision backup accounts on a node designated as a backup server, using exported resources. A user, home directory and SSH public key will be deployed on the server allowing the automated backups from the client.

Of these, the SSH public key needs special consideration, as this module does not automatically generate the required keys. If you do not already manage user SSH keys with Puppet, there are generally two ways to go about it, the manual way and the automated way.

In a nutshell, the manual way requires you to generate an SSH key pair on each backup client and copy the public keys to a central directory on the server, while the automated way uses the Puppet agent and the [voxpupuli/puppet-ssh_keygen](https://github.com/voxpupuli/puppet-ssh_keygen) module to generate the key pair, along with the `$ssh_keys_users` fact provided by the [shared-puppet-modules-group/sshd](https://gitlab.com/shared-puppet-modules-group/sshd) module to make the public key available in your manifests. See below for examples.

First, define a node as the backup server:

``` puppet
# on node backup.example.org
class { 'backupninja':
  server           => true,
  server_backupdir => '/data/backups',
}
```

Then, on the backup client, declare the backupninja class along with a remote backup action. Notice that the public SSH public key will be retrieved from the Puppet server.


``` puppet
# on node client.example.org
class { 'backupninja':
  account_host          => 'backup.example.org',
  server_backupdir      => '/data/backups',
  account_sshkey_source => "puppet:///modules/profiles/ssh_keys/${::fqdn}/root_id_ed25519.pub",
}
backupninja::action::rdiff { 'main':
  include => [ '/root', '/home' ],
  exclude => [ '/home/*/.cache' ],
  type    => 'remote',
}
```

If you use a module that provides the `$ssh_keys_users` fact, you can declare the backupninja class as below. Notice this method allows more flexibility since it makes use of the `ssh_authorized_key` native type.


``` puppet
# on node client.example.org
class { 'backupninja':
  account_host           => 'backup.example.org',
  server_backupdir       => '/data/backups',
  account_sshkey         => $::ssh_keys_users['root']['id_ed25519.pub']['key'],
  account_sshkey_type    => $::ssh_keys_users['root']['id_ed25519.pub']['type'],
  account_sshkey_options => [ 'restrict' ],
}
```

If the SSH keypair for the root account is being generated using the `ssh_keygen` module, the `$ssh_keys_users` fact will not contain the root public key on the very first Puppet agent run. To work around this, wrap the class declaration in a `if has_key($::ssh_keys_users, 'root')` statement.

### Multiple server FQDNs

If the backupserver is reachable by multiple FQDNs for the same backupserver, with the clients deciding which FQDN to use, then be sure to collect the resources for the accounts with the additional FQDNs, eg. on the server:

``` puppet
    # this will realize resources exported to this server's $::fqdn
    class { 'backupninja':
      server => true
    }

    # if it's reachable via other FQDNs, you want to realize those extra ones with a statement as below
    Backupninja::Server::Account <<| tag == 'backup-private.example.net' |>>
```

And then on the clients:

``` puppet
    # on node clientA:
    class { 'backupninja':
      account_host => 'backup.example.net'
    }

    # on node clientB:
    class { 'backupninja':
      account_host => 'backup-private.example.net'
    }
```

## Reference

The full reference documentation for this module may be found at on [GitLab Pages](https://shared-puppet-modules-group.gitlab.io/backupninja).

Alternatively, you may build yourself the documentation using the `puppet strings generate` command. See the documentation for [Puppet Strings](https://puppet.com/blog/using-puppet-strings-generate-great-documentation-puppet-modules) for more information.

## Limitations

 - Puppet 4 or later is required.
 - Debian was used for testing, but the module should work on any system for which backupninja is packaged.
 - Configuration parameters related to vservers are not implemented.

### Known issues

This module does not verify your backups, check that everything you need is actually in there, or even check whether the data can actually be restored. You need to *regularly test your backups*.

I cannot stress this enough: do not wait until disaster strikes, **TEST YOUR BACKUPS**.

## Development

This module's development is tracked on GitLab. Please submit issues and merge requests on the [shared-puppet-modules-group/backupninja](https://gitlab.com/shared-puppet-modules-group/backupninja/) project page.

Upstream backupninja development happens at [riseuplabs/backupninja](https://0xacab.org/riseuplabs/backupninja/) on [0xacab.org](https://0xacab.org)
