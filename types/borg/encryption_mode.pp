# A enum of valid borg encryption modes
# https://borgbackup.readthedocs.io/en/stable/usage/init.html#encryption-modes
type Backupninja::Borg::Encryption_mode = Enum[
  'none', 'repokey', 'keyfile', 'authenticated', 'authenticated-blake2', 'repokey-blake2', 'keyfile-blake2'
]
