require 'spec_helper'

describe 'backupninja' do
  context 'supported operating systems' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) { facts }

        context 'with defaults' do
          it { is_expected.to create_class('backupninja') }
          it { is_expected.to contain_class('backupninja::install') }
          it { is_expected.to contain_class('backupninja::config') }
          it { is_expected.to contain_class('backupninja::cron') }
          it { is_expected.to compile.with_all_deps }
        end

        context 'backupninja::install' do
          context 'with defaults' do
            it { is_expected.to contain_package('backupninja') }
          end

          context 'manage_packages set to false' do
            let(:params) { { manage_packages: false } }
            it { is_expected.not_to contain_package('backupninja') }
          end
        end

        context 'backupninja::config' do
          context 'with defaults' do
            it { is_expected.to contain_file('/etc/backup.d').with_ensure('directory') }
            it { is_expected.to contain_file('/etc/backupninja.conf').with_content(%r{^# This file is managed by Puppet\.$}) }
          end

          context 'with purge_configdirectory set to true' do
            let(:params) { { purge_configdirectory: true } }
            it {
              is_expected.to contain_file('/etc/backup.d').with(
                :purge => true,
                :recurse => true,
              )
            }
          end

          context 'with reportsuccess set to true' do
            let(:params) { { reportsuccess: true } }
            it { is_expected.to contain_file('/etc/backupninja.conf').with_content(%r{^reportsuccess = yes$}) }
          end

          context 'when reportemail is set' do
            let(:params) { { reportemail: 'backupreports@example.com' } }
            it { is_expected.to contain_file('/etc/backupninja.conf').with_content(%r{^reportemail = backupreports@example\.com$}) }
          end

          context 'when program_paths is set' do
            let(:params) { { program_paths: { :RSYNC => '/usr/local/bin/rsync' } } }
            it { is_expected.to contain_file('/etc/backupninja.conf').with_content(%r{^RSYNC=/usr/local/bin/rsync$}) }
          end
        end

        context 'backupninja::cron' do
          context 'with defaults' do
            it { is_expected.to contain_backupninja__cron__task('default') }
          end

          context 'with manage_cron set to false' do
            let(:params) { { manage_cron: false } }
            it { is_expected.not_to contain_backupninja__cron__task('default') }
          end

          context 'with cron_ensure set to absent' do
            let(:params) { { cron_ensure: 'absent' } }
            it { is_expected.to contain_backupninja__cron__task('default').with_ensure('absent') }
          end
        end

        context 'backupninja::server' do
          context 'with defaults' do
            it { is_expected.not_to contain_class('backupninja::server') }
          end

          context 'when server set to true' do
            let(:params) { { server: true } }
            it { is_expected.to contain_class('backupninja::server') }
            it { is_expected.to contain_group('backupninjas').with_gid(700) }
            it {
              is_expected.to contain_file('/backup').with(
                :ensure => 'directory',
                :mode   => '0710',
                :owner  => 'root',
                :group  => 'backupninjas',
              ).that_requires('Group[backupninjas]')
            }
          end
        end

        context 'when account_host is set' do
          let(:params) { { account_host: 'backups.example.com' } }

          it {
            expect(exported_resources).to contain_backupninja__server__account("#{facts[:fqdn]}@#{params[:account_host]}").with(
              :user   => "backup-#{facts[:hostname]}",
              :gid    => 'backupninjas',
              :home   => "/backup/#{facts[:fqdn]}",
              :sshdir => "/backup/#{facts[:fqdn]}/.ssh",
              :tag    => params[:account_host],
            )
          }
        end

      end
    end
  end
end
