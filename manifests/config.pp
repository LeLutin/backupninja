# This class handles the backupninja configuration file.
#
# @private
#
class backupninja::config {

  assert_private()

  file {
    default:
      owner => 'root',
      group => 'root';
    $backupninja::configdirectory:
      ensure  => directory,
      mode    => '0750',
      recurse => $backupninja::purge_configdirectory,
      purge   => $backupninja::purge_configdirectory;
    $backupninja::configfile:
      content => epp('backupninja/conf.epp'),
      mode    => '0644';
  }

}
