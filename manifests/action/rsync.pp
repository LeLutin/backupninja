# Run rsync as part of a backupninja run.
#
define backupninja::action::rsync (
  String $ensure                                   = 'present',
  Integer $order                                   = 90,
  Optional[Boolean] $validate                      = undef,
  # general handler config
  Optional[String] $when                           = undef,
  Stdlib::Absolutepath $log                        = '/var/log/backup/rsync.log',
  Optional[Stdlib::Absolutepath] $partition        = undef,
  Boolean $fscheck                                 = false,
  Boolean $read_only                               = false,
  Optional[Stdlib::Absolutepath] $mountpoint       = $backupninja::_account_home,
  String $backupdir                                = $name,
  Enum['short', 'long', 'mirror'] $format          = 'short',
  Integer[1] $days                                 = 7,
  Integer $keepdaily                               = 5,
  Integer $keepweekly                              = 3,
  Integer $keepmonthly                             = 1,
  Integer $nicelevel                               = 0,
  Boolean $enable_mv_timestamp_bug                 = false,
  Stdlib::Absolutepath $tmp                        = '/tmp',
  Boolean $multiconnection                         = false,
  # source
  Enum['local', 'remote'] $from                    = 'local',
  Optional[String] $source_host                    = undef,
  Optional[String] $source_port                    = undef,
  Optional[String] $source_user                    = undef,
  Optional[Boolean] $source_testconnect            = undef,
  Optional[Array[String]] $include                 = undef,
  Optional[Array[String]] $exclude                 = undef,
  Optional[String] $source_ssh                     = undef,
  Optional[Enum['ssh', 'rsync']] $source_protocol  = undef,
  Optional[String] $rsync                          = undef,
  Optional[String] $rsync_options                  = '-av --delete --recursive',
  Boolean $source_numericids                       = false,
  Boolean $source_compress                         = false,
  Optional[Integer] $source_bandwidthlimit         = undef,
  Optional[String] $source_remote_rsync            = undef,
  Optional[Stdlib::Absolutepath] $source_id_file   = '/root/.ssh/id_ed25519',
  Boolean $source_batch                            = false,
  Optional[Stdlib::Absolutepath] $source_batchbase = undef,
  Boolean $filelist                                = false,
  Optional[Stdlib::Absolutepath] $filelistbase     = undef,
  # dest
  Enum['local', 'remote'] $dest                   = 'remote',
  Optional[String] $dest_host                     = $backupninja::account_host,
  Optional[String] $dest_port                     = undef,
  Optional[String] $dest_user                     = $backupninja::account_user,
  Optional[Boolean] $dest_testconnect             = undef,
  Optional[String] $dest_ssh                      = undef,
  Optional[Enum['ssh', 'rsync']] $dest_protocol   = undef,
  Boolean $dest_numericids                        = false,
  Boolean $dest_compress                          = false,
  Optional[Integer] $dest_bandwidthlimit          = undef,
  Optional[String] $dest_remote_rsync             = undef,
  Optional[Stdlib::Absolutepath] $dest_id_file    = '/root/.ssh/id_ed25519',
  Boolean $dest_batch                             = false,
  Optional[Stdlib::Absolutepath] $dest_batchbase  = undef,
  # services
  Optional[Stdlib::Absolutepath] $initscripts     = undef,
  Optional[String] $service                       = undef,
  # system
  Optional[String] $rm                            = undef,
  Optional[String] $cp                            = undef,
  Optional[String] $touch                         = undef,
  Optional[String] $mv                            = undef,
  Optional[String] $fsck                          = undef,
) {

  # install client dependencies
  if $backupninja::manage_packages {
    ensure_packages(['rsync'], {'ensure' => $backupninja::ensure_rdiffbackup_version})
  }

  if $from == 'remote' and $dest != 'local' {
      fail('When source is remote, destination should be local.')
  }

  if empty($include) {
    fail('Must include one or more directories to backup!')
  }

  if empty($mountpoint) {
    fail('Must define a mountpoint to store the backup!')
  }

  if $from == 'remote' {
    $_source_compress    = $source_compress
    $_source_id_file     = $source_id_file
  }
  else {
    $_source_compress    = undef
    $_source_id_file     = undef
  }

  if $dest == 'remote' {
    if (empty($dest_host) or empty($dest_user)) {
      fail('Must define dest_host and dest_user for remote backup')
    }
    $_dest_host        = $dest_host
    $_dest_user        = $dest_user
    $_dest_compress    = $dest_compress
    $_dest_id_file     = $dest_id_file
  }
  else {
    $_dest_host        = undef
    $_dest_user        = undef
    $_dest_compress    = undef
    $_dest_id_file     = undef
  }

  $config = {
    'when'     => $when,
    'general'  => {
      'log'                     => $log,
      'partition'               => $partition,
      'fscheck'                 => $fscheck,
      'read_only'               => $read_only,
      'mountpoint'              => $mountpoint,
      'backupdir'               => $backupdir,
      'format'                  => $format,
      'days'                    => $days,
      'keepdaily'               => $keepdaily,
      'keepmonthly'             => $keepmonthly,
      'nicelevel'               => $nicelevel,
      'enable_mv_timestamp_bug' => $enable_mv_timestamp_bug,
      'tmp'                     => $tmp,
      'multiconnection'         => $multiconnection,
    },
    'source'   => {
      'from'           => $from,
      'host'           => $source_host,
      'port'           => $source_port,
      'user'           => $source_user,
      'testconnect'    => $source_testconnect,
      'include'        => $include,
      'exclude'        => $exclude,
      'ssh'            => $source_ssh,
      'protocol'       => $source_protocol,
      'rsync'          => $rsync,
      'rsync_options'  => $rsync_options,
      'numericids'     => $source_numericids,
      'compress'       => $_source_compress,
      'bandwidthlimit' => $source_bandwidthlimit,
      'remote_rsync'   => $source_remote_rsync,
      'id_file'        => $_source_id_file,
      'batch'          => $source_batch,
      'batchbase'      => $source_batchbase,
      'filelist'       => $filelist,
      'filelistbase'   => $filelistbase,
    },
    'dest'     => {
      'dest'           => $dest,
      'host'           => $_dest_host,
      'port'           => $dest_port,
      'user'           => $_dest_user,
      'testconnect'    => $dest_testconnect,
      'ssh'            => $dest_ssh,
      'protocol'       => $dest_protocol,
      'numericids'     => $dest_numericids,
      'compress'       => $_dest_compress,
      'bandwidthlimit' => $dest_bandwidthlimit,
      'remote_rsync'   => $dest_remote_rsync,
      'id_file'        => $_dest_id_file,
      'batch'          => $dest_batch,
      'batchbase'      => $dest_batchbase,
    },
    'services' => {
      'initscripts' => $initscripts,
      'service'     => $service,
    },
    'system'   => {
      'rm'    => $rm,
      'cp'    => $cp,
      'touch' => $touch,
      'mv'    => $mv,
      'fsck'  => $fsck,
    },
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'rsync',
    content  => epp('backupninja/action.epp', { 'config' => $config }),
    validate => $validate,
  }

}
